# builder.sh

# This script creates a directory in the build output ($out) and writes a "Hello, World!" message to a file inside that directory.

mkdir -p $out
echo "Hello, World!" > $out/hello.txt
