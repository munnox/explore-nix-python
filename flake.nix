{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        inherit (poetry2nix.legacyPackages.${system}) mkPoetryApplication;
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = {
          myapp = mkPoetryApplication { projectDir = self; };
          # myapp = poetry2nix.mkPoetryApplication {
          #     projectDir = ./.;
          # };
          # myscripts = poetry2nix.mkPoetryScriptsPackage {
          #     projectDir = ./.;
          #     python = pkgs.python310;
          # };
          default = self.packages.${system}.myapp;
          # default = pkgs.python310;
        };

        # This expression uses stdenv.mkDerivation to define a derivation.
        # It specifies a builder script (builder.sh), and the source (src)
        # is fetched from a URL using fetchurl.
        # TODO cannot seem to find the mkDerivation when running 'nix run .#simple'
        packages.simple = pkgs.lib.mkDerivation {
          name = "hello";
          builder = ./builder.sh;
          # src = fetchurl {
          #   url = https://example.com/hello.txt;
          #   sha256 = "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef";
          # };
        };

        devShells.default = pkgs.mkShell {
          packages = [ poetry2nix.packages.${system}.poetry ];
        };
      });
}
